----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    01:31:33 01/27/2019 
-- Design Name: 
-- Module Name:    control - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity control is
    Port ( sys_clk : in  std_logic;
           --rst : in  std_logic;
           led : out  std_logic_vector(3 downto 0) := (others => '0');
		   io_port_2 : out std_logic_vector(14 downto 0);
		   io_port_2_rx: in std_logic);
end control;

architecture rtl of control is
	component DCM
	port(
		CLKIN_IN : in std_logic;          
		CLKFX_OUT : out std_logic;
		LOCKED_OUT : out std_logic
	);
	end component;

	component glitch
	port(
		clk : in std_logic;
		run : in std_logic;
		delay : in std_logic_vector(31 downto 0);
		duration : in std_logic_vector(31 downto 0);
		stoped : out std_logic;
		target : out std_logic
	);
	end component;

	component uart_tx
	port(
		clk : in std_logic;
		data : in std_logic_vector(7 downto 0);
		tx_start : in std_logic;          
		tx_line : out std_logic;
		tx_done : out std_logic
	);
	end component;

	component uart_rx
	port(
		clk : in std_logic;
		rx_line : in std_logic;      
		data : out std_logic_vector(7 downto 0);
		rx_avail : out std_logic;
		rx_fault : out std_logic
	);
	end component;
	
	component rom
	port(
		clka : in std_logic;
		addra : in std_logic_vector(11 downto 0);
		douta : out std_logic_vector(7 downto 0)
	);
	end component;
	
	component cfg_ram
	port(
		clka : in std_logic;
		wea : in std_logic_vector(0 downto 0);
		addra : in std_logic_vector(11 downto 0);
		dina : in std_logic_vector(7 downto 0);
		douta : out std_logic_vector(7 downto 0)
	);
	end component;

	signal hs_clk : std_logic;
	signal dcm_locked : std_logic;
	
	signal dbg_cntr_led : std_logic := '0';
	signal dbg_cntr : integer range 0 to 100000000 := 0;
	signal glitch_delay : std_logic_vector(31 downto 0) := (others => '0');
	signal glitch_duration : std_logic_vector(31 downto 0) := (others => '0');
	signal glitch_done : std_logic;
	signal glitch_start : std_logic := '0';
	signal glitch_target : std_logic;
	
	signal tx_byte : std_logic_vector(7 downto 0) := "00101001";
	signal tx_pin : std_logic := '1';
	signal tx_start : std_logic := '0';
	
	signal rx_byte : std_logic_vector(7 downto 0);
	signal rx_avail : std_logic;
	signal rx_pin : std_logic;
	signal rx_state_fail : std_logic;
	
	constant ROM_GREETING : std_logic_vector(11 downto 0) := x"000";
	constant ROM_HELLO : std_logic_vector(11 downto 0) := x"400";
	constant ROM_FAULT : std_logic_vector(11 downto 0) := x"600";
	constant ROM_DONE : std_logic_vector(11 downto 0) := x"800";
	signal rom_addr : std_logic_vector(11 downto 0) := ROM_GREETING;
	signal rom_byte : std_logic_vector(7 downto 0);
	
	constant RAM_CFG_DELAY : std_logic_vector(11 downto 0) := x"000";
	constant RAM_CFG_DURATION : std_logic_vector(11 downto 0) := x"400";
	constant RAM_CFG_TMP : std_logic_vector(11 downto 0) := x"800";
	signal ram_addr : std_logic_vector(11 downto 0) := (others => '0');
	signal ram_rd : std_logic_vector(7 downto 0);
	signal ram_wr : std_logic_vector(7 downto 0);
	signal ram_we : std_logic_vector(0 downto 0);

	signal ctrl_cntr : integer range 0 to 100000000 := 0;
	signal dbg_out : std_logic_vector(7 downto 0) := (others => '0');
	type ctrl_state_t is (STATE_GREETING, STATE_RESET, STATE_HELLO, STATE_RX_DELAY_LEN, STATE_RX_DURATION_LEN, STATE_READ_DELAY_LEN, STATE_READ_DURATION_LEN, STATE_GLITCH, STATE_DONE);
	signal ctrl_state : ctrl_state_t := STATE_GREETING;
	signal byte_cntr : integer range 0 to 7 := 0;
	signal cfg_value : std_logic_vector(31 downto 0);
begin
	led(0) <= not glitch_target;
	led(2) <= dbg_cntr_led;
	led(3) <= dbg_cntr_led;
	dbg_out <= rx_byte;
	io_port_2(6 downto 0) <= (0 => tx_pin, 1 => tx_pin, 2 => tx_pin, others => '0');
	io_port_2(14 downto 7) <= dbg_out(7 downto 0);
	rx_pin <= io_port_2_rx;

	Inst_DCM: DCM port map(
		CLKIN_IN => sys_clk,
		CLKFX_OUT => hs_clk,
		LOCKED_OUT => dcm_locked
	);

	Inst_glitch: glitch port map(
		clk => hs_clk,
		run => glitch_start,
		delay => glitch_delay,
		duration => glitch_duration,
		stoped => glitch_done,
		target => glitch_target
	);
	
	Inst_uart_tx: uart_tx port map(
		clk => hs_clk,
		data => tx_byte,
		tx_start => tx_start,
		tx_line => tx_pin
	);
	
	Inst_uart_rx: uart_rx port map(
		clk => hs_clk,
		data => rx_byte,
		rx_avail => rx_avail,
		rx_line => rx_pin,
		rx_fault => rx_state_fail
	);
	
	Inst_rom : rom port map(
		clka => hs_clk,
		addra => rom_addr,
		douta => rom_byte
	);
	
	Inst_cfg_ram : cfg_ram port map (
		clka => hs_clk,
		wea => ram_we,
		addra => ram_addr,
		dina => ram_wr,
		douta => ram_rd
	);
		
	debug : process(hs_clk)
		variable curr_ram_addr : std_logic_vector(11 downto 0) := (others => '0');
		variable skip_one_cycle : std_logic := '0';
		
		procedure proc_ram_rd_val is
		begin
			ram_addr <= curr_ram_addr + byte_cntr + 1;
			case byte_cntr is
				when 0 =>
					cfg_value(31 downto 24) <= ram_rd;
				when 1 =>
					cfg_value(23 downto 16) <= ram_rd;
				when 2 =>
					cfg_value(15 downto 8) <= ram_rd;
				when 3 =>
					cfg_value(7 downto 0) <= ram_rd;
					byte_cntr <= 0;
				when others =>
					ram_addr <= curr_ram_addr;
			end case;
			byte_cntr <= byte_cntr + 1;
		end proc_ram_rd_val;
		
		procedure proc_uart_print is
		begin
			if ctrl_cntr = 1000000 then
				if rom_byte = x"00" then
					ctrl_cntr <= 0;
				else
					tx_byte <= rom_byte;
					rom_addr <= rom_addr + 1;
					tx_start <= '1';
				end if;
				ctrl_cntr <= 0;
			else
				tx_byte <= (others => '0');
				tx_start <= '0';
				ctrl_cntr <= ctrl_cntr + 1;
			end if;
		end proc_uart_print;
	begin
		if falling_edge(hs_clk) then
			case ctrl_state is
				when STATE_GREETING =>
					if skip_one_cycle = '1' then
						proc_uart_print;
						if rom_byte = x"00" then
							byte_cntr <= 0;
							ctrl_state <= STATE_RESET;
						end if;
					end if;
					skip_one_cycle := '1';
				when STATE_RESET =>
					-- TODO reset target
					if rx_avail = '1' AND rx_byte = x"0a" then
						ctrl_state <= STATE_HELLO;
						rom_addr <= ROM_HELLO;
					end if;
				when STATE_HELLO =>
					proc_uart_print;
					if rom_byte = x"00" then
						byte_cntr <= 0;
						ctrl_state <= STATE_RX_DELAY_LEN;
					end if;
				when STATE_RX_DELAY_LEN =>
					if rx_state_fail = '1' then
						ctrl_state <= STATE_HELLO;
					end if;
					if rx_avail = '1' then
						ram_addr <= RAM_CFG_DELAY + byte_cntr;
						ram_wr <= rx_byte;
						ram_we(0) <= '1';
						byte_cntr <= byte_cntr + 1;
						if byte_cntr = 3 then
							ctrl_state <= STATE_RX_DURATION_LEN;
							byte_cntr <= 0;
						end if;
					end if;
				when STATE_RX_DURATION_LEN =>
					if rx_state_fail = '1' then
						ctrl_state <= STATE_HELLO;
					end if;
					if rx_avail = '1' then
						ram_addr <= RAM_CFG_DURATION + byte_cntr;
						ram_wr <= rx_byte;
						ram_we(0) <= '1';
						byte_cntr <= byte_cntr + 1;
						if byte_cntr = 4 then
							ctrl_state <= STATE_READ_DELAY_LEN;
							ram_addr <= RAM_CFG_DELAY;
							byte_cntr <= 0;
							ram_we(0) <= '0';
							ctrl_cntr <= 0;
						end if;
					end if;
				when STATE_READ_DELAY_LEN =>
					-- test 5s/10s 29 205 101 00 59 154 202 29
					curr_ram_addr := RAM_CFG_DELAY;
					proc_ram_rd_val;
					if byte_cntr = 4 then
						glitch_delay <= cfg_value;
						byte_cntr <= 0;
						ctrl_state <= STATE_READ_DURATION_LEN;
						ram_addr <= RAM_CFG_DURATION;
					end if;
				when STATE_READ_DURATION_LEN =>
					curr_ram_addr := RAM_CFG_DURATION;
					proc_ram_rd_val;
					if byte_cntr = 4 then
						glitch_duration <= cfg_value;
						byte_cntr <= 0;
						ctrl_state <= STATE_GLITCH;
						glitch_start <= '1';
						led(1) <= '1';
					end if;
				when STATE_GLITCH =>
					-- TODO reset target release
					glitch_start <= '0';
					if glitch_done = '1' then
						ctrl_state <= STATE_DONE;
						led(1) <= '0';
						rom_addr <= ROM_DONE;
						ctrl_cntr <= 0;
					end if;
				when STATE_DONE =>
					proc_uart_print;
					if rom_byte = x"00" then
						ctrl_state <= STATE_RESET;
						rom_addr <= ROM_HELLO;
					end if;
				when others =>
			end case;

			if dbg_cntr = 50000000 then
				dbg_cntr <= 0;
				dbg_cntr_led <= not dbg_cntr_led;
			else
				dbg_cntr <= dbg_cntr + 1;
			end if;
		end if;
	end process;
end rtl;

