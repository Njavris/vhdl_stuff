----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    19:51:33 01/27/2019 
-- Design Name: 
-- Module Name:    uart_tx - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity uart_tx is
    Port ( clk : in  std_logic;
           data : in  std_logic_vector(7 downto 0);
		   tx_start : in std_logic;
           tx_line : out  std_logic;
           tx_done : out  std_logic := '1');
end uart_tx;

architecture rtl of uart_tx is
	signal tx_data : std_logic_vector(7 downto 0);
	signal curr_bit : integer range 0 to 9 := 0;
	signal clk_cntr : integer range 0 to 868 := 0; -- 100000000 / 115200 = 868,05
	--signal clk_cntr : integer range 0 to 10417 := 0; -- 100000000 / 9600 = 10416,6
	
	type state_t is (STATE_IDLE, STATE_TX);
	signal state : state_t := STATE_IDLE;
begin
	uart_tx_p : process(clk)
	begin
		if falling_edge(clk) then
			case state is
				when STATE_IDLE =>
					tx_line <= '1';
					if tx_start = '1' then
						tx_data <= data;
						curr_bit <= 0;
						state <= STATE_TX;
					end if;
				when STATE_TX =>
					tx_done <= '0';
					clk_cntr <= clk_cntr + 1;
					if clk_cntr = 868 then
						clk_cntr <= 0;
						
						if curr_bit = 0 then
							tx_line <= '0';
						elsif curr_bit = 9 then
							tx_line <= '1';
							tx_done <= '1';
							state <= STATE_IDLE;
						else
							tx_line <= tx_data(curr_bit - 1);
						end if;
						curr_bit <= curr_bit + 1;
					end if;
				when others =>
					state <= STATE_IDLE;
			end case;
		end if;
	end process;

end rtl;

