----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    20:28:59 01/28/2019 
-- Design Name: 
-- Module Name:    uart_rx - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity uart_rx is
    port ( clk : in  std_logic;
           data : out std_logic_vector(7 downto 0) := (others => '0');
           rx_avail : out  std_logic := '0';
           rx_line : in  std_logic;
		   rx_fault : out std_logic);
end uart_rx;

architecture rtl of uart_rx is
	signal curr_bit : integer range 0 to 9 := 0;
	signal clk_cntr : integer range 0 to 868 := 0; -- 100000000 / 115200 = 868,05
	--signal clk_cntr : integer range 0 to 10417 := 0; -- 100000000 / 9600 = 10416,6

	type state_t is (STATE_WAITING, STATE_RX_START, STATE_RX, STATE_AVAILABLE);
	signal state : state_t := STATE_WAITING;
begin
	uart_rx_p : process(clk)
	begin
		if falling_edge(clk) then
			case state is
				when STATE_WAITING =>
					rx_fault <= '0';
					rx_avail <= '0';
					if rx_line = '0' then
						curr_bit <= 0;
						data <= (others => '0');
						state <= STATE_RX_START;
						clk_cntr <= 0;
					end if;
				when STATE_RX_START =>
					-- start bit middle
					if clk_cntr = 434 then
						if rx_line /= '0' then
							-- something went wrong
							rx_fault <= '1';
							state <= STATE_WAITING;
						end if;
						clk_cntr <= 0;
						state <= STATE_RX;
						curr_bit <= 1;
					end if;
					clk_cntr <= clk_cntr + 1;
				when STATE_RX =>
					clk_cntr <= clk_cntr + 1;
					if clk_cntr = 868 then
						clk_cntr <= 0;
						if curr_bit = 9 then
							state <= STATE_AVAILABLE;
						else
							data(curr_bit - 1) <= rx_line;
						end if;
						curr_bit <= curr_bit + 1;
					end if;
				when STATE_AVAILABLE =>
					rx_avail <= '1';
					state <= STATE_WAITING;
				when others =>
					state <= STATE_WAITING;
			end case;
		end if;
	end process;

end rtl;

