----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    22:41:48 01/26/2019 
-- Design Name: 
-- Module Name:    glitch - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity glitch is
	port(
		clk : in std_logic;
		run : in std_logic;
		delay : in std_logic_vector(31 downto 0);
		duration : in std_logic_vector(31 downto 0);
		stoped : out std_logic;
		target : out std_logic := '0');
end glitch;

architecture rtl of glitch is
	signal delay_cntr : std_logic_vector(31 downto 0) := (others => '0'); -- delay before glitch attempt
	signal duration_cntr : std_logic_vector(31 downto 0) := (others => '0'); -- glitch duration

	-- glitch state
	type state_t is (STATE_STOPED, STATE_STARTED, STATE_SPIKE);
	signal state : state_t := STATE_STOPED;
begin
	glitch_p : process(clk, run)
	begin
		if falling_edge(clk) then
			case state is
				when STATE_STOPED =>
					stoped <= '0';
					if run = '1' then
						delay_cntr <= delay;
						duration_cntr <= duration;
						stoped <= '0';
						state <= STATE_STARTED;
					end if;
				when STATE_STARTED =>
					delay_cntr <= delay_cntr - 1;
					if delay_cntr = 0 then
						state <= STATE_SPIKE;
						target <= '1';
					end if;
				when STATE_SPIKE =>
					duration_cntr <= duration_cntr - 1;
					if duration_cntr = 0 then
						state <= STATE_STOPED;
						target <= '0';
						stoped <= '1';
					end if;
				when others =>
					state <= STATE_STOPED;
			end case;
		end if;
	end process;
end rtl;

